insert into region
(name) values
  ('Kiev region'),
  ('Lviv region'),
  ('Dnipro region'),
  ('Odessa region');

  insert into city
  (name, region_id) values
    ('Kyiv', 1),
    ('Brovary', 1),
    ('Fastov', 1),
    ('Berezan', 1),

    ('Lviv', 2),
    ('Drohobych', 2),
    ('Truskavets', 2),
    ('Sambir', 2),

    ('Pavlograd', 3),
    ('Dnipro', 3),
    ('Ternivka', 3),
    ('Apostolove', 3),

    ('Ismail', 4),
    ('Bolgrad', 4),
    ('Illichivsk', 4),
    ('Odessa', 4);


insert into street
(name, city_id) values
  ('Doroshenko str.', 1),
  ('Kerch str.', 1),
  ('Desnianskaya str.', 1),
  ('Khreshchatyk str.', 1),

  ('Ivan Vygovsky str.', 2),
  ('Gajdamatcka str.', 2),
  ('Gaunts of Ivan str.', 2),

  ('Dragomanova str.', 3),
  ('Danylo Halytskyi str.', 3),
  ('Zelena str.', 3),

  ('Leo Tolstoy str.', 4),
  ('Vyacheslav Chornovil str.', 4),

  ('Zelena str.', 5),
  ('Vyacheslav Chornovil str.', 5),
  ('Dragomanova str.', 5),
  ('Stepan Bandera str.', 5),

  ('UPA soldiers str.', 6),
  ('Gajdamatcka str.', 6),
  ('Gogol Nicholas str.', 6),
  ('Stepan Bandera str.', 6),
  ('Danylo Halytskyi str.', 6),

  ('Stepan Bandera str.', 7),
  ('Danylo Halytskyi str.', 7),
  ('Alexander Dovzhenko str.', 7),

  ('Alexander Dovzhenko str.', 8),
  ('Stepan Bandera str.', 8),

  ('Stepan Bandera str.', 9),
  ('Bohun Ivan str.', 9),
  ('Vernadsky Vladimir str.', 9),
  ('Ivan Vygovsky str.', 9),
  ('Hrushevsky Michael str.', 9),

  ('UPA soldiers str.', 10),
  ('Gajdamatcka str.', 10),
  ('Gogol Nicholas str.', 10),
  ('Stepan Bandera str.', 10),
  ('Danylo Halytskyi str.', 10),

  ('Ivan Vygovsky str.', 11),
  ('Gajdamatcka str.', 11),
  ('Gaunts of Ivan str.', 11),
  ('Svobody Ave', 11),
  ('Teatralna', 11),

  ('Doroshenko str.', 12),
  ('Kerch str.', 12),
  ('Desnianskaya str.', 12),

  ('Leo Tolstoy str.', 13),
  ('Vyacheslav Chornovil str.', 13),
  ('UPA soldiers str.', 13),

  ('Svobody Ave', 14),
  ('Stepan Bandera str.', 14),
  ('Gajdamatcka str.', 14),
  ('Doroshenko str.', 14),

  ('UPA soldiers str.', 15),
  ('Gajdamatcka str.', 15),
  ('Gogol Nicholas str.', 15),
  ('Ivan Mazepa str.', 15),
  ('Heroes of Kruty str.', 15),

  ('Heroes of Kruty str.', 16),
  ('Deribasovskaya str.', 16),
  ('Zelena str.', 16),
  ('Ivan Mazepa str.', 16),
  ('Kniazia Romana str.', 16);


  insert into person
  (name, last_name, phone_number) values
  ('Ihor', 'Bendor', '0671222236'),
  ('Sergiy', 'Levush', '0671235236'),
  ('Yuriy', 'Sorros', '0671200216'),
  ('Grits', 'Irish', '0671293612'),
  ('Oleh', 'Note', '0671231977'),
  ('Petro', 'Lee', '0671231977'),
  ('Andrew', 'Orkes', '0671212356');

  insert into item
  (sender_id, customer_id, identifier, arrival_time, departure_time, city_id, delivery_status) values
  (3,5, '1111111', '2020-03-04 22:23:00', '2020-07-06 20:20:00', 7, 'INPROCESS'),
  (2,1, '2222222', '2020-03-07 10:10:00', '2020-07-06 20:20:00', 4, 'INPROCESS'),
  (1,1, '3333333', '2020-03-06 22:23:00', '2020-07-07 20:20:00', 11, 'INPROCESS'),
  (3,5, '1111111', '2020-03-06 22:23:00', '2020-07-08 20:20:00', 2, 'INPROCESS'),
  (3,5, '1111111', '2020-03-02 22:23:00', '2020-07-10 20:20:00', 8, 'INPROCESS'),
  (3,5, '1111111', '2020-03-01 22:23:00', '2020-08-16 20:20:00', 3, 'INPROCESS'),
  (3,5, '1111111', '2020-08-15 22:23:00', '2020-08-16 20:20:00', 3, 'INPROCESS');

  insert into product
  (product_name, description, price, item_id ) values
  ('Pen', 'black pen', 24.40, 3),
  ('Laptop', 'silver', 18500, 6),
  ('Phone', 'IPhone', 21000, 7),
  ('Bike', 'Asd', 15000, 4),
  ('wire', 'iron', 50, 5),
  ('Casino', 'Light', 22, 2);