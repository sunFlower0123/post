package com.epam.model.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class ProductDto {

    private Long id;

    private String productName;

    private String description;

    private BigDecimal price;

    @JsonBackReference
    private OrderDto order;

}
