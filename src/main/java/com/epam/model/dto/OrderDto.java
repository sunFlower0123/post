package com.epam.model.dto;

import com.epam.model.enums.DeliveryStatus;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class OrderDto {

    private Long id;

    private PersonDto customer;

    private PersonDto sender;

    private String deliveryAddress;

    @JsonManagedReference
    private List<ProductDto> products;

    private String identifier;

    private LocalDateTime departureTime;

    private LocalDateTime arrivalTime;

    private DeliveryStatus deliveryStatus;

}
