package com.epam.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 35)
    private String name;

    @Column(name = "last_name", nullable = false, length = 35)
    private String lastName;

    @Column(name = "phone_number", nullable = false, length = 35)
    private String phoneNumber;

}
