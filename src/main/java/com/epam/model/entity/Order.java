package com.epam.model.entity;

import com.epam.model.enums.DeliveryStatus;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Table(name = "item")
@Entity(name = "Order")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private Person customer;

    @ManyToOne
    @JoinColumn(name = "sender_id", nullable = false)
    private Person sender;

    @ManyToOne
    @JoinColumn(name = "city_id", nullable = false)
    private City deliveryAddress;

    @OneToMany(mappedBy = "order")
    @JsonManagedReference
    @ToString.Exclude
    private List<Product> products;

    @Column(name = "identifier", nullable = false, length = 50)
    private String identifier;

    @Column(name = "departure_time", nullable = false)
    private LocalDateTime departureTime;

    @Column(name = "arrival_time", nullable = false)
    private LocalDateTime arrivalTime;

    @Column(name = "delivery_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private DeliveryStatus deliveryStatus;

}