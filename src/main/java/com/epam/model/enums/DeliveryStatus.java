package com.epam.model.enums;

public enum DeliveryStatus {

    INPROCESS, DELIVERED

}
